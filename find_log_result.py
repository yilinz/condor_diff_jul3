import os
import glob
import re

# 定义搜索模式和文件夹路径
log_folder = './log/'  # 根据你的实际情况修改路径
output_folder = './output_log/'  # 输出文件夹路径
os.makedirs(output_folder, exist_ok=True)

# 搜索模式和输出文件名的对应关系
search_patterns = {
    "MyConverge": "MyConverge.txt",
    "Full, accurate covariance matrix AND MINIMIZE=0 HESSE=0": "AccurateCovariance.txt",
    "STATUS=OK AND MINIMIZE=0 HESSE=0": "StatusOK00.txt",
    "STATUS=OK AND Full, accurate covariance matrix": "AccurateStatusOK.txt"
}

# 定义提取 minimzed FCN value 的函数
def extract_fcn_value(line):
    match = re.search(r'minimized FCN value:\s*([0-9.e+-]+)', line)
    if match:
        return float(match.group(1))
    return float('inf')  # 无法提取值时，返回无穷大，便于排序时放在最后

# 定义搜索函数
def search_and_write(patterns, output_file):
    results = []
    for log_file in glob.glob(os.path.join(log_folder, '*.log')):
        with open(log_file, 'r') as file:
            content = file.read()
            if all(pattern in content for pattern in patterns):
                lines = content.splitlines()
                for line in lines:
                    if "RooFitResult:" in line:
                        results.append((log_file, line, extract_fcn_value(line)))
                        break  # 只取第一个匹配到的结果

    # 按 minimized FCN value 升序排序
    results.sort(key=lambda x: x[2])

    # 写入输出文件
    with open(os.path.join(output_folder, output_file), 'w') as output:
        for result in results:
            output.write(f"{result[0]}: {result[1]}\n")

# 执行搜索并写入文件
search_and_write(["MyConverge"], search_patterns["MyConverge"])
search_and_write(["Full, accurate covariance matrix", "MINIMIZE=0 HESSE=0"], search_patterns["Full, accurate covariance matrix AND MINIMIZE=0 HESSE=0"])
search_and_write(["STATUS=OK", "MINIMIZE=0 HESSE=0"], search_patterns["STATUS=OK AND MINIMIZE=0 HESSE=0"])
search_and_write(["STATUS=OK", "Full, accurate covariance matrix"], search_patterns["STATUS=OK AND Full, accurate covariance matrix"])

print("处理完成，结果已保存到输出文件夹中。")
