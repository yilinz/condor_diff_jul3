#ifndef UNI_SPS_DPS_FCN_H
#define UNI_SPS_DPS_FCN_H
void UniSpsDpsFcn();
double Poly(double x, double mth, double beta, double p0, double p1, double p2, double p3, double p4);
double ExpPoly(double x, double mth, double a, double beta, double p0, double p1, double p2, double p3, double p4);
double ExpPolyWithPower(double x, double mth, double a, double beta, double alpha, double p0, double p1, double p2, double p3, double p4); 
#endif
