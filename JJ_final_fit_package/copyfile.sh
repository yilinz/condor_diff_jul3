#!/bin/bash

SOURCE_PATH="/afs/cern.ch/user/y/yilinz/WORK/CMSSW_11_1_1/src/plusSplot"

DESTINATION_PATH="/afs/cern.ch/user/y/yilinz/work/JJ_final_fit_package"

FILENAME_LIST="/afs/cern.ch/user/y/yilinz/work/JJ_final_fit_package/filename.txt"

LOG_FILE="/afs/cern.ch/user/y/yilinz/work/JJ_final_fit_package/logfile.txt"

mkdir -p "$DESTINATION_PATH"
exec 3>&1 1>>"$LOG_FILE" 2>&1

while IFS= read -r file; do
    if [[ -f "${SOURCE_PATH}/${file}" ]]; then
        cp "${SOURCE_PATH}/${file}" "${DESTINATION_PATH}/" && echo "Successfully copied: $file" || echo "Failed to copy: $file"
    else
        echo "File not found: $file"
    fi
done < "$FILENAME_LIST"

exec 1>&3 3>&-
echo "Copy operation completed. Check the log file at $LOG_FILE for details."
