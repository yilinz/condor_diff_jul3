#include "UniSpsDpsFcn.h"
#include "TMath.h"
#include <cmath>

void UniSpsDpsFcn() 
{
  return;
}
double Poly(double x, double mth, double beta, double p0, double p1, double p2, double p3, double p4) 
{
  double mx = x - mth;
  double fth = 0;
  if (mx <= 0) return fth;
  //if (mx > 0) fth = TMath::Sqrt(mx);
  if (mx > 0) fth = TMath::Power(mx, beta);
  //double p1 = -(p0 + p4 + p2 + p3);
  double poly = p0 + p1 * mx + p2 * mx * mx + p3 * mx * mx * mx + p4 * mx * mx * mx * mx;
  //double res = fth * poly;
  double res = poly;
  return res;
}

double ExpPoly(double x, double mth, double a, double beta, double p0, double p1, double p2, double p3, double p4) 
{
  double mx = x - mth;
  double fth = 0;
  if (mx <= 0) return fth;
  //if (mx > 0) fth = TMath::Sqrt(mx);
  if (mx > 0) fth = TMath::Power(mx, beta);
  //double exponent = TMath::Exp(-1 * a * mx);
  double exponent = TMath::Exp(-1 * a * fth);
  //double p1 = -(p0 + p2 + p3 + p4);
  double poly = p0 + p1 * mx + p2 * mx * mx + p3 * mx * mx * mx + p4 * mx * mx * mx * mx;
  //double res = fth * exponent * poly;
  double res = exponent * poly;
  return res;
}

double ExpPolyWithPower(double x, double mth, double a, double beta, double alpha, 
                        double p0, double p1, double p2, double p3, double p4) 
{
  double mx = x - mth;
  double fth = 0, powTerm = 1;
  if (mx <= 0) return fth;
  if (mx > 0) 
  {
    fth = TMath::Power(mx, beta);
    powTerm = TMath::Power(mx, alpha);  // Additional power term
  }
  double exponent = TMath::Exp(-1 * a * fth);
  //double p1 = -(p0 + p2 + p3 + p4);
  double poly = p0 + p1 * mx + p2 * mx * mx + p3 * mx * mx * mx + p4 * mx * mx * mx * mx;
  double res = fth * exponent * poly * powTerm;  // Multiply with the additional power term
  return res;
}

