#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooHistFunc.h"
#include "RooAddPdf.h"
#include "RooEffProd.h"
#include "RooProdPdf.h"
#include "RooFFTConvPdf.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooProduct.h"
#include "RooRandom.h"
#include "RooHist.h"
#include "TLine.h"
#include "RooCBShape.h"

#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TH1F.h"
#include "TLegend.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TMath.h"
#include "TStyle.h"
#include "TString.h"
#include "TRandom3.h"
#include "TFrame.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

#include "RooRealVar.h"
#include "RooStats/SPlot.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooBreitWigner.h"
#include "RooChebychev.h"
#include "RooAddPdf.h"
#include "RooAddition.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooProdPdf.h"
#include "RooProduct.h"
#include "RooCBShape.h"
#include "TSystem.h"
#include "RooFitResult.h"

#include "TStyle.h"


using namespace std;
using namespace RooFit;
using namespace RooStats;

void print(string, vector<double>);
void print_vectors(string, vector<double>, vector<double>);
void drawVectors( vector<double>, vector<double>);

double effectiveBins(double mth, double mLow, double mUp, double mBins) {
  double mBin0 = 0;
  double binWidth = (mUp - mLow) / mBins;
  while (mBin0 < mBins) {
    double mx = mLow + (mBin0 + 1) * binWidth;
    if (mx > mth) break;
    mBin0 += 1;
  }
  double effBins = mBins - mBin0;
  return effBins;
}
//jpsi_pt
void pre_check_distribution_toyMC() {

    
    const double PI = TMath::Pi();
    double mxMin = 6, mxMax = 15;
    //100 - 90 MeV/bin; 150 - 60 MeV/bin; 300 - 30 MeV/bin
    int mxBins = 200;//(15-6.6)*1000/40 
    double mxBinWidth = (mxMax - mxMin) / mxBins;
    TString YTitle, XTitle;
    YTitle.Form("Candidates / %d MeV", int(mxBinWidth * 1000 + 0.5));
    XTitle.Form("m(J/#psi J/#psi) (GeV)");
    double center = (mxMin + mxMax)/2.0;
    double shift = -center;
    int STRATEGY = 1; //0, 1, 2
    int NCPU = 4; //num of cpus
    int FFT_BINS = 10000;
    bool MINOS = false;
    const int COUT_PRECISION = 6;
    double effectiveBins(double mth, double mLow, double mUp, double mBins);

    //const double MTH = 3.6861 + 3.096900;////////move by Gu
    //RooRealVar R_MTH("R_MTH", "R_MTH", MTH);
    //RooRealVar R_MUPdata("R_MUPdata", "R_MUPdata", 1.40496e+01/*1.24878e+01*/);///////move by Gu
    //RooRealVar R_MUPcomb("R_MUPcomb", "R_MUPcomb",9.43573e+00);///////move by Gu    

        
    float jpsi1_mass_Min = 2.7, jpsi1_mass_Max = 3.5;
    float jpsi2_mass_Min = 2.7, jpsi2_mass_Max = 3.5;

    
    vector<double> x_MC;
    vector<double> y_MC;
    vector<double> x_Data;
    vector<double> y_Data;
    vector<double> numS;
    vector<double> numB;
    bool flag = true;

    // RooRealVar runNum("runNum", "runNum", 0, 10000000000);
    // RooRealVar lumiNum("lumiNum", "lumiNum", 0, 10000000000);
    // RooRealVar evtNum("evtNum", "evtNum", 0, 100000000000);

    // RooRealVar old_tri("old_tri", "old_tri", -2, 2);
    // RooRealVar new_tri("new_tri", "new_tri", -2, 2);

    RooRealVar mx("mx", "mx", 6., 15.);
    //RooRealVar X_pt("X_pt", "X_pt", 0, 1000);

    // RooRealVar jpsi1_mass("jpsi1_mass", "m_{(#mu^{+}#mu^{-})_{1}} GeV", 2.7, 3.5);
    // RooRealVar jpsi2_mass("jpsi2_mass",  "m_{(#mu^{+}#mu^{-})_{2}} GeV", 2.7, 3.5);
    // RooRealVar jpsi1_pt("jpsi1_pt", "jpsi1_pt", -1000, 1000);
    // RooRealVar jpsi2_pt("jpsi2_pt", "jpsi2_pt", -1000, 1000);
    // RooRealVar jpsi1_eta("jpsi1_eta", "jpsi1_eta", -1000, 1000);
    // RooRealVar jpsi2_eta("jpsi2_eta", "jpsi2_eta", -1000, 1000);

    // RooRealVar mu1_pt("mu1_pt", "mu1_pt", -1000, 1000);
    // RooRealVar mu2_pt("mu2_pt", "mu2_pt", -1000, 1000);
    // RooRealVar mu3_pt("mu3_pt", "mu3_pt", -1000, 1000);
    // RooRealVar mu4_pt("mu4_pt", "mu4_pt", -1000, 1000);
    // RooRealVar mu1_eta("mu1_eta", "mu1_eta", -1000, 1000);
    // RooRealVar mu2_eta("mu2_eta", "mu2_eta", -1000, 1000);
    // RooRealVar mu3_eta("mu3_eta", "mu3_eta", -1000, 1000);
    // RooRealVar mu4_eta("mu4_eta", "mu4_eta", -1000, 1000);

    // RooRealVar jpsi1_px("jpsi1_px", "jpsi1_px", -1000, 1000);
    // RooRealVar jpsi1_py("jpsi1_py", "jpsi1_py", -1000, 1000);
    // RooRealVar jpsi1_pz("jpsi1_pz", "jpsi1_pz", -1000, 1000);
    // RooRealVar jpsi1_E("jpsi1_E", "jpsi1_E", -1000, 1000);
    // RooRealVar jpsi2_px("jpsi2_px", "jpsi2_px", -1000, 1000);
    // RooRealVar jpsi2_py("jpsi2_py", "jpsi2_py", -1000, 1000);
    // RooRealVar jpsi2_pz("jpsi2_pz", "jpsi2_pz", -1000, 1000);
    // RooRealVar jpsi2_E("jpsi2_E", "jpsi2_E", -1000, 1000);
    
    // RooRealVar mu1_px("mu1_px", "mu1_px", -1000, 1000);
    // RooRealVar mu1_py("mu1_py", "mu1_py", -1000, 1000);
    // RooRealVar mu1_pz("mu1_pz", "mu1_pz", -1000, 1000);
    // RooRealVar mu1_E("mu1_E", "mu1_E", -1000, 1000);
    // RooRealVar mu2_px("mu2_px", "mu2_px", -1000, 1000);
    // RooRealVar mu2_py("mu2_py", "mu2_py", -1000, 1000);
    // RooRealVar mu2_pz("mu2_pz", "mu2_pz", -1000, 1000);
    // RooRealVar mu2_E("mu2_E", "mu2_E", -1000, 1000);
    // RooRealVar mu3_px("mu3_px", "mu3_px", -1000, 1000);
    // RooRealVar mu3_py("mu3_py", "mu3_py", -1000, 1000);
    // RooRealVar mu3_pz("mu3_pz", "mu3_pz", -1000, 1000);
    // RooRealVar mu3_E("mu3_E", "mu3_E", -1000, 1000);
    // RooRealVar mu4_px("mu4_px", "mu4_px", -1000, 1000);
    // RooRealVar mu4_py("mu4_py", "mu4_py", -1000, 1000);
    // RooRealVar mu4_pz("mu4_pz", "mu4_pz", -1000, 1000);
    // RooRealVar mu4_E("mu4_E", "mu4_E", -1000, 1000);

        


        RooArgSet variables;
        // variables.add(runNum);
        // variables.add(lumiNum);
        // variables.add(evtNum);

        // variables.add(old_tri);
        // variables.add(new_tri);

        variables.add(mx);
        //variables.add(X_pt);
        //variables.add(jpsi1_mass);
        //variables.add(jpsi2_mass);
        // variables.add(jpsi1_pt);
        // variables.add(jpsi2_pt);
        // variables.add(jpsi1_eta);
        // variables.add(jpsi2_eta);

        // variables.add(mu1_pt);
        // variables.add(mu2_pt);
        // variables.add(mu3_pt);
        // variables.add(mu4_pt);

        // variables.add(mu1_eta);
        // variables.add(mu2_eta);
        // variables.add(mu3_eta);
        // variables.add(mu4_eta);

        // variables.add(jpsi1_px);
        // variables.add(jpsi1_py);
        // variables.add(jpsi1_pz);
        // variables.add(jpsi1_E);

        // variables.add(jpsi2_px);
        // variables.add(jpsi2_py);
        // variables.add(jpsi2_pz);
        // variables.add(jpsi2_E);

        // variables.add(mu1_px);
        // variables.add(mu1_py);
        // variables.add(mu1_pz);
        // variables.add(mu1_E);

        // variables.add(mu2_px);
        // variables.add(mu2_py);
        // variables.add(mu2_pz);
        // variables.add(mu2_E);

        // variables.add(mu3_px);
        // variables.add(mu3_py);
        // variables.add(mu3_pz);
        // variables.add(mu3_E);

        // variables.add(mu4_px);
        // variables.add(mu4_py);
        // variables.add(mu4_pz);
        // variables.add(mu4_E);


        //RooDataSet data = *RooDataSet::read("../data/txtfile_May23/Run3toyMC_Run2data_1.txt", variables, "Q");
        RooDataSet data = *RooDataSet::read("../data/txtfile_Jun19/toyMC_RunII_7.txt", variables, "Q");
        // RooDataSet data_2 = *RooDataSet::read("../data/signalMC2022/wimasswin_mx_6900.txt", variables, "Q");


        TCanvas *canvas = new TCanvas("canvas", "Variable Histograms", 800, 600);

        // 0. X_mass
        // TH1F *hist_X_mass = new TH1F("hist_X_mass", "X_mass Distribution; m_{(#mu^{+}#mu^{-})_{1}(#mu^{+}#mu^{-})_{2}} GeV; Events", 360, 6, 15);
        // data.fillHistogram(hist_X_mass, RooArgList(mx));
        // hist_X_mass->SetStats(kFALSE);
        // hist_X_mass->SetLineColor(kRed);
        // hist_X_mass->Scale(1.0 / hist_X_mass->Integral());
        // 0. X_mass
        TH1F *hist_X_mass = new TH1F("hist_X_mass", "X_mass Distribution; m_{(#mu^{+}#mu^{-})_{1}(#mu^{+}#mu^{-})_{2}} GeV; Events", 360, 6, 15);
        data.fillHistogram(hist_X_mass, RooArgList(mx));
        hist_X_mass->Draw();
        canvas->SaveAs("./figure/X_mass_toyMC.pdf");

        // TH1F *hist_X_mass_2 = new TH1F("hist_X_mass_2", "X_mass Distribution; m_{(#mu^{+}#mu^{-})_{1}(#mu^{+}#mu^{-})_{2}} GeV; Events", 120, 6, 9);
        // data.fillHistogram(hist_X_mass_2, RooArgList(mx));
        // hist_X_mass_2->Draw();
        // canvas->SaveAs("../figure/X_mass_ALLcheck_2_May23_4.pdf");
        //hist_X_mass->SetStats(kFALSE);
        //hist_X_mass->SetLineColor(kRed);
        //hist_X_mass->Scale(1.0 / hist_X_mass->Integral());

        // TH1F *hist_X_mass_2 = new TH1F("hist_X_mass_2", "X_mass_2 Distribution; m_{(#mu^{+}#mu^{-})_{1}(#mu^{+}#mu^{-})_{2}} GeV; Events", 100, 6.7, 7.1);
        // data_2.fillHistogram(hist_X_mass_2, RooArgList(mx));
        // hist_X_mass_2->SetStats(kFALSE);
        // hist_X_mass_2->SetLineColor(kBlue);
        // hist_X_mass_2->Scale(1.0 / hist_X_mass_2->Integral());

        // hist_X_mass->Draw("HIST");
        // hist_X_mass_2->Draw("HIST SAME");
        // // 添加图例
        // TLegend *legend = new TLegend(0.7, 0.7, 0.9, 0.9);
        // legend->AddEntry(hist_X_mass, "X6900_MC_2022EE", "l");
        // legend->AddEntry(hist_X_mass_2, "X6900_MC_2022GS", "l");
        // legend->Draw();
        // // canvas->SaveAs("../figure/X_mass_SPStoJJ_Direct_2022EE_May23.pdf");
        // canvas->SaveAs("../figure/X_mass_X6900_compare_May23.pdf");
        // delete hist_X_mass;

        // TH1F *hist_X_pT = new TH1F("hist_X_pT", "hist_X_pT; X pT GeV; Events", 200, 0, 80);
        // data.fillHistogram(hist_X_pT, RooArgList(X_pt));
        // hist_X_pT->Draw();
        // canvas->SaveAs("hist_X_pT.pdf");
        //delete hist_jpsi1_mass;


/*        // 1. jpsi2_mass
        TH1F *hist_jpsi1_mass = new TH1F("hist_jpsi1_mass", "jpsi1_mass Distribution; m_{(#mu^{+}#mu^{-})_{1}} GeV; Events", 400, 2.7, 3.5);
        data.fillHistogram(hist_jpsi1_mass, RooArgList(jpsi1_mass));
        hist_jpsi1_mass->Draw();
        canvas->SaveAs("../figure/jpsi1_mass_Run2_2dfit.pdf");
        //delete hist_jpsi1_mass;
        TH1F *hist_jpsi1_mass_rightzoomed = new TH1F("hist_jpsi1_massrightzoomed", "jpsi1_mass Distribution rightzoomed; m_{(#mu^{+}#mu^{-})_{1}} GeV; Events", 100, 3.2, 3.5);
        data.fillHistogram(hist_jpsi1_mass_rightzoomed, RooArgList(jpsi1_mass));
        hist_jpsi1_mass_rightzoomed->Draw();
        canvas->SaveAs("../figure/jpsi1_mass_Run2_2dfit_rightzoomed.pdf");
        TH1F *hist_jpsi1_mass_leftzoomed = new TH1F("hist_jpsi1_massleftzoomed", "jpsi1_mass Distribution leftzoomed; m_{(#mu^{+}#mu^{-})_{2}} GeV; Events", 100, 2.7, 3.0);
        data.fillHistogram(hist_jpsi1_mass_leftzoomed, RooArgList(jpsi1_mass));
        hist_jpsi1_mass_leftzoomed->Draw();
        canvas->SaveAs("../figure/jpsi1_mass_Run2_2dfit_leftzoomed.pdf");


        // 1. jpsi2_mass
        TH1F *hist_jpsi2_mass = new TH1F("hist_jpsi2_mass", "jpsi2_mass Distribution; m_{(#mu^{+}#mu^{-})_{2}} GeV; Events", 400, 2.7, 3.5);
        data.fillHistogram(hist_jpsi2_mass, RooArgList(jpsi2_mass));
        hist_jpsi2_mass->Draw();
        canvas->SaveAs("../figure/jpsi2_mass_Run2_2dfit.pdf");
        //delete hist_jpsi2_mass;
        TH1F *hist_jpsi2_mass_rightzoomed = new TH1F("hist_jpsi2_massrightzoomed", "jpsi2_mass Distribution rightzoomed; m_{(#mu^{+}#mu^{-})_{2}} GeV; Events", 100, 3.2, 3.5);
        data.fillHistogram(hist_jpsi2_mass_rightzoomed, RooArgList(jpsi2_mass));
        hist_jpsi2_mass_rightzoomed->Draw();
        canvas->SaveAs("../figure/jpsi2_mass_Run2_2dfit_rightzoomed.pdf");
        TH1F *hist_jpsi2_mass_leftzoomed = new TH1F("hist_jpsi2_massleftzoomed", "jpsi2_mass Distribution leftzoomed; m_{(#mu^{+}#mu^{-})_{2}} GeV; Events", 100, 2.7, 3.0);
        data.fillHistogram(hist_jpsi2_mass_leftzoomed, RooArgList(jpsi2_mass));
        hist_jpsi2_mass_leftzoomed->Draw();
        canvas->SaveAs("../figure/jpsi2_mass_Run2_2dfit_leftzoomed.pdf");
*/
/*
        // 2. jpsi1_pt
        TH1F *hist_jpsi1_pt = new TH1F("hist_jpsi1_pt", "jpsi1_pt Distribution; jpsi1_pt; Events", 100, 0, 30);
        data.fillHistogram(hist_jpsi1_pt, RooArgList(jpsi1_pt));
        hist_jpsi1_pt->Draw();
        canvas->SaveAs("jpsi1_pt_histogram_ChitoJP_loose.pdf");
        //delete hist_jpsi1_pt;

        // 3. jpsi2_pt
        TH1F *hist_jpsi2_pt = new TH1F("hist_jpsi2_pt", "jpsi2_pt Distribution; jpsi2_pt; Events", 100, 0, 30);
        data.fillHistogram(hist_jpsi2_pt, RooArgList(jpsi2_pt));
        hist_jpsi2_pt->Draw();
        hist_jpsi1_pt->SetLineColor(8);
        hist_jpsi1_pt->Draw("same");

        TLegend *legend_3 = new TLegend(0.7, 0.7, 0.9, 0.9);
        legend_3->AddEntry(hist_jpsi2_pt, "jpsi2_pt", "l");
        legend_3->AddEntry(hist_jpsi1_pt, "jpsi1_pt", "l");
        legend_3->Draw();
        canvas->SaveAs("jpsi2_pt_histogram_ChitoJP_loose.pdf");
        //delete hist_jpsi2_pt;

        // 4. jpsi1_eta
        TH1F *hist_jpsi1_eta = new TH1F("hist_jpsi1_eta", "jpsi1_eta Distribution; jpsi1_eta; Events", 100, -5, 5);
        data.fillHistogram(hist_jpsi1_eta, RooArgList(jpsi1_eta));
        hist_jpsi1_eta->Draw();
        canvas->SaveAs("jpsi1_eta_histogram_ChitoJP_loose.pdf");
        //delete hist_jpsi1_eta;

        // 5. jpsi2_eta
        TH1F *hist_jpsi2_eta = new TH1F("hist_jpsi2_eta", "jpsi2_eta Distribution; jpsi2_eta; Events", 100, -5, 5);
        data.fillHistogram(hist_jpsi2_eta, RooArgList(jpsi2_eta));
        hist_jpsi2_eta->Draw();
        hist_jpsi1_eta->SetLineColor(8);
        hist_jpsi1_eta->Draw("same");
         
        TLegend *legend_2 = new TLegend(0.7, 0.7, 0.9, 0.9);
        legend_2->AddEntry(hist_jpsi2_eta, "jpsi2_eta", "l");
        legend_2->AddEntry(hist_jpsi1_eta, "jpsi1_eta", "l");
        legend_2->Draw();
        canvas->SaveAs("jpsi2_eta_histogram_ChitoJP_loose.pdf");
        //delete hist_jpsi2_eta;

        // 6. mu1_pt
        TH1F *hist_mu1_pt = new TH1F("hist_mu1_pt", "mu1_pt Distribution; mu1_pt; Events", 100, 0, 30);
        data.fillHistogram(hist_mu1_pt, RooArgList(mu1_pt));
        hist_mu1_pt->Draw();
        canvas->SaveAs("mu1_pt_histogram_ChitoJP_loose.pdf");
       // delete hist_mu1_pt;

        // 7. mu2_pt
        TH1F *hist_mu2_pt = new TH1F("hist_mu2_pt", "mu2_pt Distribution; mu2_pt; Events", 100, 0, 30);
        data.fillHistogram(hist_mu2_pt, RooArgList(mu2_pt));
        hist_mu2_pt->Draw();
        canvas->SaveAs("mu2_pt_histogram_ChitoJP_loose.pdf");
       // delete hist_mu2_pt;

        // 8. mu3_pt
        TH1F *hist_mu3_pt = new TH1F("hist_mu3_pt", "mu3_pt Distribution; mu3_pt; Events", 100, 0, 30);
        data.fillHistogram(hist_mu3_pt, RooArgList(mu3_pt));
        hist_mu3_pt->Draw();
        canvas->SaveAs("mu3_pt_histogram_ChitoJP_loose.pdf");
       // delete hist_mu3_pt;

        // 9. mu4_pt
        TH1F *hist_mu4_pt = new TH1F("hist_mu4_pt", "mu4_pt Distribution; mu4_pt; Events", 100, 0, 30);
        data.fillHistogram(hist_mu4_pt, RooArgList(mu4_pt));
        hist_mu4_pt->Draw();
        hist_mu3_pt->SetLineColor(2);
        hist_mu3_pt->Draw("same");
        hist_mu2_pt->SetLineColor(8);
        hist_mu2_pt->Draw("same");
        hist_mu1_pt->SetLineColor(6);
        hist_mu1_pt->Draw("same");

        TLegend *legend_1 = new TLegend(0.7, 0.7, 0.9, 0.9);
        legend_1->AddEntry(hist_mu4_pt, "mu4_pt", "l");
        legend_1->AddEntry(hist_mu3_pt, "mu3_pt", "l");
        legend_1->AddEntry(hist_mu2_pt, "mu2_pt", "l");
        legend_1->AddEntry(hist_mu1_pt, "mu1_pt", "l");
        legend_1->Draw();

        canvas->SaveAs("mu4_pt_histogram_ChitoJP_loose.pdf");
        //delete hist_mu4_pt;

        // 10. mu1_eta
        TH1F *hist_mu1_eta = new TH1F("hist_mu1_eta", "mu1_eta Distribution; mu1_eta; Events", 100, -5, 5);
        data.fillHistogram(hist_mu1_eta, RooArgList(mu1_eta));
        hist_mu1_eta->Draw();
        canvas->SaveAs("mu1_eta_histogram_ChitoJP_loose.pdf");
      //  delete hist_mu1_eta;

        // 11. mu2_eta
        TH1F *hist_mu2_eta = new TH1F("hist_mu2_eta", "mu2_eta Distribution; mu2_eta; Events", 100, -5, 5);
        data.fillHistogram(hist_mu2_eta, RooArgList(mu2_eta));
        hist_mu2_eta->Draw();
        canvas->SaveAs("mu2_eta_histogram_ChitoJP_loose.pdf");
       // delete hist_mu2_eta;

        // 12. mu3_eta
        TH1F *hist_mu3_eta = new TH1F("hist_mu3_eta", "mu3_eta Distribution; mu3_eta; Events", 100, -5, 5);
        data.fillHistogram(hist_mu3_eta, RooArgList(mu3_eta));
        //hist_mu3_eta->SetLineColor(kRed);
        hist_mu3_eta->Draw();
        canvas->SaveAs("mu3_eta_histogram_ChitoJP_loose.pdf");
       // delete hist_mu3_eta;

        gStyle->SetOptStat(0);  // 

        // 13. mu4_eta
        TH1F *hist_mu4_eta = new TH1F("hist_mu4_eta", "mu4_eta Distribution; mu4_eta; Events", 100, -5, 5);
        data.fillHistogram(hist_mu4_eta, RooArgList(mu4_eta));
        hist_mu4_eta->Draw();
        hist_mu3_eta->SetLineColor(2);
        hist_mu3_eta->Draw("same");
        hist_mu2_eta->SetLineColor(8);
        hist_mu2_eta->Draw("same");
        hist_mu1_eta->SetLineColor(6);
        hist_mu1_eta->Draw("same");

        TLegend *legend = new TLegend(0.7, 0.7, 0.9, 0.9);
        legend->AddEntry(hist_mu4_eta, "mu4_eta", "l");
        legend->AddEntry(hist_mu3_eta, "mu3_eta", "l");
        legend->AddEntry(hist_mu2_eta, "mu2_eta", "l");
        legend->AddEntry(hist_mu1_eta, "mu1_eta", "l");
        legend->Draw();

        canvas->SaveAs("mu4_eta_histogram_ChitoJP_loose.pdf");
        delete hist_mu4_eta;
        delete canvas;
        */

}

void print(string str, vector<double> vec){
    
    if(vec.size()>0){
        cout << str << " : ";
        
        for(long unsigned int j=0; j<vec.size(); j++){
            cout << vec[j] << ", ";
        }
        
        cout << endl;
    }
}

void print_vectors(string str, vector<double> vec1, vector<double> vec2){
    
    if(vec1.size()>0 && vec2.size()>0){
        
        double max_vec2 = vec2[0];
        double cor_vec1 = vec1[0];
        cout << str << " : " << endl;
        
        for(long unsigned int j=0; j<vec1.size(); j++){
            
            cout << vec1[j] << ", ";
        }

        cout << endl;
        
        for(long unsigned int j=0; j<vec2.size(); j++){
            cout << vec2[j] << ", ";

            if( vec2[j]>max_vec2 ){
                max_vec2 = vec2[j];
                cor_vec1 = vec1[j];
            }
        }
        cout << endl << str << "Max = " << cor_vec1 << " : " << max_vec2 << endl;
    }
}

void drawVectors( vector<double> x, vector<double> y){
    TCanvas *canvas = new TCanvas("c1","A Simple Graph Example",200,10,700,500);
    TGraph *gr = new TGraph(x.size(), &x[0], &y[0]);
    gr->SetTitle("Figure of Merit");
    gr->GetXaxis()->SetTitle("p_{T}(/#psi(2S)) GeV");
    //gr->GetYaxis()->SetTitle("#frac{S}{#frac{S}{#frac{463}{13}+4#sqrt{B}+5#sqrt{25+8#sqrt{B}+4B}}}");
    gr->GetYaxis()->SetTitle("FOM");
    gr->Draw("AL");
    gr->SetLineWidth(7);

}
