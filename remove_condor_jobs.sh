#!/bin/bash

# 设置开始和结束的序号
start=60140882
end=60141410
onejob=32723874
jobstart=200
jobend=399

# 循环通过所有序号
# for (( i=start; i<=end; i++ )); do
for (( i=jobstart; i<=jobend; i++ )); do
    condor_rm -name lpcschedd5.fnal.gov ${onejob}.${i}
done

echo "All specified condor jobs have been removed."
