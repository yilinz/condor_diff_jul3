import os
import re

# 设置工作目录和输出文件名
work_dir = './log/'  # 修改为你的日志文件夹路径
output_file = 'output.txt'

# 正则表达式匹配
status_pattern = re.compile(r'Status\s*:\s*MINIMIZE=0\s*HESSE=0')
distance_pattern = re.compile(r'estimated distance to minimum:\s*(\d+\.\d+)')
fcn_value_pattern = re.compile(r'minimized FCN value:\s*(\d+\.\d+)')

# 搜索并处理文件
results = []

for filename in os.listdir(work_dir):
    if filename.startswith("interf_plusfd_Comb_1_") and filename.endswith("C.log"):
        filepath = os.path.join(work_dir, filename)
        with open(filepath, 'r') as file:
            content = file.read()
            # 检查文件内容是否符合条件
            if status_pattern.search(content):
                distance_match = distance_pattern.search(content)
                if distance_match and float(distance_match.group(1)) < 0.001:
                    fcn_value_match = fcn_value_pattern.search(content)
                    if fcn_value_match:
                        results.append((filename, fcn_value_match.group(1)))

# 将结果写入输出文件
with open(output_file, 'w') as file:
    for filename, fcn_value in results:
        file.write(f"{filename}: minimized FCN value: {fcn_value}\n")

print(f"Done. Found {len(results)} matching files. Results written to {output_file}.")

