#!/bin/bash

log_directory="./log"
output_log="output_matches.log"
pdf_log="output_pdf_names.log"

> "$output_log"
> "$pdf_log"

for file in "$log_directory"/interf_plusfd_Comb_*_*.C.log; do
	filename=$(basename "$file")
  if grep -q "MyConverge" "$file"; then
  	grep "MyConverge" "$file" | while read -r line; do
  		echo "$file: $line" >> "$output_log"
    done
        
        numbers=($(echo "$filename" | sed -E 's/interf_plusfd_Comb_([0-9]+)_([0-9]+)\.C\.log/\1 \2/'))
        num1="${numbers[0]}"
        num2="${numbers[1]}"

        new_pdf_name="fitRes_${num2}_0_toy${num1}.pdf"
        echo "$new_pdf_name" >> "$pdf_log"
    fi
done

