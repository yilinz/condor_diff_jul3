#!/bin/bash

### Now that the cmsRun is over, there are one or more root files created
echo "What directory am I in?"
pwd
echo "List all root files = "
ls *.root
echo "List all files"
ls -alh
echo "*******************************************"

# Define base directory
BASEDIR=/uscms/home/yilinz/nobackup/CMSSW_11_1_1/src/Condor_New

# Define remote directories
REMOTE_BASE=root://cmseos.fnal.gov//store/user/yilinz/CMSSW_11_1_1/src/for_Condor
LOGDIR=${REMOTE_BASE}/log
FIGUREDIR=${REMOTE_BASE}/figure
TXTDIR=${REMOTE_BASE}/output

echo "xrdcp output for condor to remote directories"
echo "Log directory: $LOGDIR"
echo "Figure directory: $FIGUREDIR"
echo "Txt directory: $TXTDIR"

# Move to the base directory
cd $BASEDIR

# Assuming root files are in output and need to be copied to log
for FILE in output/*.txt
do
  echo "xrdcp -f ${FILE} ${TXTDIR}/$(basename ${FILE})"
  xrdcp -f ${FILE} ${TXTDIR}/$(basename ${FILE}) 2>&1
  XRDEXIT=$?
  if [[ $XRDEXIT -ne 0 ]]; then
    echo "exit code $XRDEXIT, failure in xrdcp"
    #exit $XRDEXIT
  fi
  # rm ${FILE}
done

# Assuming log files are in log and need to be copied
for FILE in log/*.log
do
  echo "xrdcp -f ${FILE} ${LOGDIR}/$(basename ${FILE})"
  xrdcp -f ${FILE} ${LOGDIR}/$(basename ${FILE}) 2>&1
  XRDEXIT=$?
  if [[ $XRDEXIT -ne 0 ]]; then
    echo "exit code $XRDEXIT, failure in xrdcp"
    #exit $XRDEXIT
  fi
  # rm ${FILE}
done

# Assuming figure files are in figure and need to be copied
for FILE in figure/*.pdf
do
  echo "xrdcp -f ${FILE} ${FIGUREDIR}/$(basename ${FILE})"
  xrdcp -f ${FILE} ${FIGUREDIR}/$(basename ${FILE}) 2>&1
  XRDEXIT=$?
  if [[ $XRDEXIT -ne 0 ]]; then
    echo "exit code $XRDEXIT, failure in xrdcp"
    #exit $XRDEXIT
  fi
  # rm ${FILE}
done
