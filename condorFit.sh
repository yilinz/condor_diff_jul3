#!/bin/bash

fitPackage=JJ_final_fit_package
##arguement {1} is script/doFit_xx_xx.C if queue fitfile matching files fitdir/fit*C
scriptDir=`echo ${1}|cut -d"/" -f1` # script/
fitScript=`echo ${1}|cut -d"/" -f2` # doFit_xx_xx.C

echo "argument is ${1}"

cd ${fitPackage}
cp ../${scriptDir}/${fitScript} .
#cp ../${1} . 


#to use roofit in CMSSW, 11_1_1 and later for roofit with fft
 echo "==   Load CMSSW environment  =="
 export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
 source ${VO_CMS_SW_DIR}/cmsset_default.sh
###cmsrel CMSSW_11_1_1 #this shortcut command is not avaialbe in shell script
 export SCRAM_ARCH=slc7_amd64_gcc820
 scramv1 project CMSSW CMSSW_11_1_1
 cd CMSSW_11_1_1/src/
 eval `scramv1 runtime -sh`
 cd ../..

#create output dirs
#rm -rf output figure
mkdir -p output
mkdir -p figure
mkdir -p log

##compile user defined fit functions
./makeLibSo.sh

####if ./data/ exists
#set correct path in myFit/doFit.C and do not need below lines
#if [ -d data ] ; then 
#  cp ../data/* . 
#fi

echo "will do root -l -b -q ${fitScript}"
#root -l -b -q "${fitScript}+" > log/${fitScript}.log
root -l -b -q "${fitScript}+" > /dev/null
echo "root -l -b -q ${fitScript}+ done"
####echo "${fitScript} Done!" >> log/${fitScript}.log
cd ..
