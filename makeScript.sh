#!/bin/bash

### STEP 1, change the data path, output path in myFit/doFit.C, which is template of fit script
###        other changes for a fit, is also need to be don in myFit/doFit.C, e.g., plot fit result with different settings, change range of free pars, etc
###        in lxplus.cern.ch condor system, you can directly write output to your /eos dsk, or directly read data from your /eos disk
###        But you can not submit ht condor jobs at /eos disk (the /eos disk doesnot support this service)
###  [1.1] put your data to be fitted in ./data/, and set the path correctly in myFit/doFit.C
###         or you can put your data in somwhere lxplus hcondor can see, e.g., /eos/xxx/xxx/, and then set the input path correctly in myFit/doFit.C


### STEP 2, change the below NJOB, STEP, IJOB_BEG, N_FIT_MAX as you like
###         NJOB is the total number of independent fit task, and they will be in njob = NJOB/NSTEP scripts and then njob condor jobs
###         STEP means one fit script will apply n=STEP independt fit tasks
###         N_FIT_MAX means each fit task will try to do at most N_FIT_MAX fit trials to get a convergef fit
###         IJOB_BEG is the begin number of fit scripts. Useful if you first make 100 fits, with IJOB_BEG = 0; then you an make another with IJOB_BEG = 100 or 200, etc, then the random seed for initial parameter value generation is different, and you get another 100 indpendent fits

### STEP 3, make fit scripts by ./makeScript.sh

### STEP 4, change condorJob.sub, e.g., the max time  of the job running, the '+JobFlavour' item


mkdir -p script
#for i in {7..7}; do
        for j in {1..500}; do
        
        # job=FinalFit_Data_combine_${i}_${j}
	job=FinalFit_Data_combine_${j}

        cp ./JJ_final_fit_package/FinalFit_Data_combine_SEED.C script/${job}.C
        sed -i "s/FinalFit_Data_combine_SEED/$job/g" script/${job}.C
        sed -i "s/JOBNUM = 0/JOBNUM = ${j}/g" script/${job}.C
       # sed -i "s/ToyMCNUM = 0/ToyMCNUM = ${i}/g" script/${job}.C
                
        done
#done
